CREALOGIX = {
    locations: [
        {
            name: 'CREALOGIX Headquater',
            address: 'Baslerstrasse 60, 8048 Zürich'
        },
        {
            name: 'CREALOGIX (Deutschland) AG',
            address: 'Breitscheidstraße 10, 70174 Stuttgart, Germany'
        },
        {
            name: 'CREALOGIX UK LTD',
            address: 'Staple Gardens, Winchester SO23 8SR, UK'
        },
		{
            name: 'CREALOGIX PTE. LTD. (Singapore)',
            address: '80 Raffles Place, Singapore 048624'
        },
		{
            name: 'CREALOGIX (Austria) GmbH',
            address: 'Herrengasse 1-3, 1010 Wien, Austria'
        }
    ]
}