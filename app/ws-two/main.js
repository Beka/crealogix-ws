/**
 * Exercise number two - locationApp
 */

/**
 * Step one
 * 
 * Create a title component (the same as in excercise one)
 */
var Title = React.createClass({
  render: function() {
    return (
      React.createElement('h1', null, this.props.value)
    );
  }
});


/**
 * Step two
 * 
 * Complete the ListItem component, which is used to render each entry in the List component
 * Follow the instructions inside the component
 */
var ListItem = React.createClass({
  _showMarkerOnMap() {
    geocoder.geocode({'address': this.props.address}, function(results, status) {
      if (status == 'OK') {
        map.setCenter(results[0].geometry.location);
        
        var marker = new google.maps.Marker({
            map: map,
            position: results[0].geometry.location
        });
        marker.setMap(map);

        marker.setAnimation(google.maps.Animation.BOUNCE);

      } else {
        alert('Geocode was not successful for the following reason: ' + status);
      }
    });
  },
  render: function() {
    return (
      React.createElement('button', {className: 'list-group-item', onClick: this._showMarkerOnMap}, this.props.value)
    );
  }
});

/**
 * Step two
 *
 * Create a List component following the instructions bellow
 */
var List = React.createClass({
  render: function() {
    let htmlId = 0;
    return (
      React.createElement('div', {className: 'list-group'},
      this.props.listItems.map(function(item) {
        htmlId++;
        return React.createElement(ListItem, {key: htmlId, value: item.name, address: item.address});
      })
    ));
  }
});

/**
 * Render all the elements on the DOM
 */
ReactDOM.render(
  React.createElement('section', {className: 'col-sm-12'},
    React.createElement(Title, {value: 'Google Maps are awesome'}),
    React.createElement(List, {listItems: CREALOGIX.locations})),
  document.getElementById('locationApp')
);

