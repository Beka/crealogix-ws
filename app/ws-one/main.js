/**
 * Exercise one - Button component
 */

/**
 * Step one
 * 
 * Create a simple title component here
 * look at the readme.md for instructions
 * 
 * --------------------------------------
 * var Title = React.createClass({
 *   render: function() {
 *     return (
 *       React.createElement(html element or react component, props, children)
 *     );
 *   }
 * });
 * -------------------------------------- 
 */
var Title = React.createClass({
  render: function() {
    return (
      React.createElement('h1', null, this.props.value)
    );
  }
});

/**
 * Step two
 * 
 * Create a Button component here
 * Look at the readme.md for instructions
 * 
 * --------------------------------------
 * var Button = React.createClass({
 *   render: function() {
 *     const _clickAction = function() {
 *       An alert message should be fired from here
 *     }
 * 
 *     return (
 *		 A react createElement(component or element, props, children) should be here
 *     );
 *   }
 * });
 * --------------------------------------
 */
var Button = React.createClass({
  render: function() {
    const _clickAction = () => {
      alert(this.props.message);
    }
    return (
      React.createElement('button', {className: 'btn btn-primary', onClick: _clickAction}, this.props.value)
    );
  }
});


/**
 * Step three
 * 
 * Render everything on the DOM to the element with ID of 'buttonApp'
 * Look at the readme.md for instructions
 * 
 * --------------------------------------
 * ReactDOM.render(
 *   React.createElement('section', null,
 *     Add the title component here, 
 *     Add the button component here,
 *   Here find the element with ID buttonApp on the DOM
 * );
 * --------------------------------------
 */

ReactDOM.render(
  React.createElement('section', null, 
    React.createElement(Title, {value: 'I am a Title'}),
    React.createElement(Button, {value: 'Click me', message: 'I am a message'})
  ),
  document.getElementById('buttonApp')
);