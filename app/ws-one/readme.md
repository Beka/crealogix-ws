# Crealogix WorkShop - Excercise one: React button component
In this exercise we will create a button React component. It will have a onclick event that opens an alert window with a custom message.

## What? A React component? How should I do that?
Here is an example:

```
var TitleComponent = React.createClass({
  render: function() {
    return (
      React.createElement('h1', null, this.props.value)
    );
  }
});
```

* We use a react function "React.createClass" where we pass an object as a parameter, which must contain at least the render function
* The render function as the name suggests will render elements on the DOM
* A render function must return an element (If we would like to have multiple elements we need to wrap them in a container, which is just another element wrapping all the others)

## Ok Ok, this is simple... But how should I create a click event now?

* React.createElement function accepts the following parameters: 
    - React.createElement(component, props, ...children)
    - Name of the element (for example h1, section, div... or you can also put here a React component)
    - props -> Props are a way to pass parameters to a React component. We can pass an object containing all the props we want. (for example {name: 'Crealogix'} ... this will then be available in the component as 'this.props.name')
    - As a second parameter, together with props you can pass some html element attributes as well. (for example {className: 'btn', onClick: function(){}})
    - children -> Here we can pass as many React.createElement() or whatever we want to be inside the current element, for example simple text if the element is a paragraph or heading

### Now that we know all of that, we can guess how we can do it. Lets have a quick look:
* Inside the object which we are passing to React.createClass we will add a new function which will handle the click event:
```
const _clickAction = function () {
    console.log('click action fired! Waaa');
}
```
* Now that we have the function we can simply add the onClick event in React.createElement function:
```
React.createElement('button', {onClick: _clickAction})
```

## Cool! So now I have a React component. Ammm and what now?
Now we need to add our awesome new React component to the DOM and try it out in the browser.

In the HTML file you noticed that there is an element with an ID of 'buttonApp'. That is the element where we will render our React app with our newly created Button component.

* To render our app with components to the DOM we use ReactDOM.render function:
```
ReactDOM.render(
    React.createElement(ButtonComponent, {value: 'Click me', message: 'This is an awesome message'}),
    document.getElementById('buttonApp')
);
```

Now we can try to open ws-one.html in the browser and see the results of our hard work
